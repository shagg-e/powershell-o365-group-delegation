#Get input on credentials
$adminuser =  Read-Host "Please enter the admin user/email"

#Connect to O365 and prompt user for password
Import-Module ExchangeOnlineManagement
Connect-ExchangeOnline -UserPrincipalName $adminuser

#Start log
Start-Transcript -Path '.\LastRunLog.txt'

#Write status to console
Write-Output "*** Beginning process of adding new users to the list of mailbox permissions ***"

#Test Grabbing list of inboxes from InboxList
Write-Output "Grabbing array of inboxes that will be delegated from InboxList.txt"
$TargetInboxList = Get-Content .\InboxList.txt

#Test grabbing the name of the distribution list from DelegationGroup
Write-Output "Grabbing name of the distribution group from DelegationGroup.txt"
$DelegationGroupName = Get-Content .\DelegationGroup.txt

#Test pulling distribution group members from DelegationGroupName variable
Write-Output "Pulling the members of the distribution group which need delegation access to the inboxes"
$DelegationGroupMembers = Get-DistributionGroupMember -Identity $DelegationGroupName

#Convert $DelegationGroupMembers to email address array to use in the removal area of the script
$DelegationGroupMemberAddresses = @()
Foreach ($member in $DelegationGroupMembers){
    $DelegationGroupMemberAddresses += $member.PrimarySMTPAddress
}

#Loop through TargetInboxList and add perms for users in DelegationGroupMembers
Foreach ($member in $DelegationGroupMembers){
    Foreach($inbox in $TargetInboxList){
        Write-Output "Adding $member to have permissions over $inbox"
        Add-MailboxPermission -Identity $inbox -User $member.PrimarySMTPAddress -AccessRights FullAccess

        #This line works if client wants SendAs permissions on the mailboxes as well
        Add-RecipientPermission -Identity $inbox -AccessRights SendAs -Trustee $member.PrimarySMTPAddress -Confirm:$false
    }
}

#Write status to console
Write-Output "*** Beginning process of removing users that were removed from the delegation group from the list of delegation mailboxes ***"

#Start looping through each mailbox
Foreach ($inbox in $TargetInboxList){

    #Pull perms for current mailbox
    $PermissionsList = Get-MailboxPermission -Identity $inbox

    #Now that we have array of full access users, start looping through them
    Foreach ($PermittedMember in $PermissionsList){

        #Skip NT AUTHORITY\SELF
        if ([string]$PermittedMember.User -eq "NT AUTHORITY\SELF") {
            Write-Output "Skipping NT AUTHORITY\SELF"
        }
        #if not self, do stuff
        else{
                #Check if current PermittedMember exists in DelegationGroupMembers
                if($DelegationGroupMemberAddresses -notcontains $PermittedMember.User){
                    Write-Host "Found " -NoNewline
                    Write-Host $PermittedMember.User -NoNewline
                    Write-Host " in $inbox permissions when they shouldn't have it, removing their permissions now."

                    #Now remove the permissions of PermittedMember from the current inbox
                    #since if we are at this point, they are NOT in the DelegationGroup but
                    #they DO have permissions to the current inbox
                    Remove-MailboxPermission -Identity $inbox -User $PermittedMember.User -AccessRights FullAccess -Confirm:$false –BypassMasterAccountSid
                    Remove-RecipientPermission -Identity $inbox -AccessRights SendAs -Trustee $PermittedMember.User -Confirm:$false –BypassMasterAccountSid

                }

        }
    }

}

Stop-Transcript

Read-Host -Prompt "Press enter to exit"
Get-PSSession | Remove-PSSession